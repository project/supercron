<?php

/**
 * @file
 * Handles incoming requests to fire off regularly-scheduled tasks (cron jobs).
 */

// If you place your supercron.php in other place than Drupal root, or supercron
// directory. Specify your script here.
// $drupal_dir = '';

// Ignore user aborts and allow the script to run forever
ignore_user_abort(TRUE);

// Finding Drupal root directory
if (!isset($drupal_dir)) {
    if (!file_exists('includes/bootstrap.inc')) {
        $exp = '@^(.*)[\\\/]sites[\\\/][^\\\/]+[\\\/]modules[\\\/]supercron$@';
        preg_match($exp, getcwd(), $matches);

        if (!empty($matches)) {
            chdir($matches[1]);
        }
    }
}
else {
    chdir($drupal_dir);
}

if (!file_exists('includes/bootstrap.inc')) {
    exit('Can not find Drupal directory.');
}

// Running full Drupal bootstrap
include_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

// All cron tasks will be executed as User 1

global $user;
$original_user = $user;
session_save_session(FALSE);

$safety       = isset($_GET['safety']) ? $_GET['safety'] : '';
$valid_safety = supercron_registry()->is_valid_safety($safety);

// IP authorization check
$ip = ip_address();
if (is_null($ip)) $ip = '127.0.0.1'; // bash calls return a null calling IP
if (!supercron_registry()->is_allowed_by_firewall($ip))
    exit("IP '$ip' not authorized!");

// Throttle check
if (supercron_registry()->should_be_throttled())
    exit('Site is under heavy load; cron tasks postponed.');

supercron_registry()->set_php_binary_location($_SERVER['_']);

if ($safety) {
    if (!$valid_safety) {
        exit('Safety mismatch');
    }

    $module = isset($_GET['module']) ? $_GET['module'] : '';
    if (!module_exists($module))
        exit('Specifically-called module does not exist');

    //$user = user_load(array('uid' => 1));
    supercron_modules()->crontab_invoke_specific_module($module);

}
else {
    //$user = user_load(array('uid' => 1));
    supercron_module_invoke_all_cron();
}

$user = $original_user;
session_save_session(TRUE);