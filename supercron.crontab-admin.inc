<?php

/**
 * @file
 * Contains administrative configuration helper for Cron tasks
 */


function supercron_invocation_form(&$form_state) {
    $supercron_module_path = supercron_registry()->module_location;
    drupal_add_js("{$supercron_module_path}/js/ZeroClipboard.js");
    drupal_add_js("{$supercron_module_path}/js/supercron.crontab.js");
    drupal_add_js(array('supercron_module_path' => $supercron_module_path), 'setting');

    $exe = supercron_registry()->get_php_binary_path_description($exefound);

    $supercron_script_path_type = supercron_registry()->get_script_path_value();
    $form['invocation'][supercron_registry_class::prefs_controller_path] = array(
            '#type'      => 'radios',
            '#title'     => t('Path to your supercron script'),
            '#required'  => TRUE,
            '#default_value' => $supercron_script_path_type,
            '#options'   => array(
                    supercron_registry_class::controller_path_is_drupal_root => t('Drupal root (@path)', array('@path' => supercron_registry()->get_standard_controller_path())),
                    supercron_registry_class::controller_path_is_module_root => t('Module root (@path)', array('@path' => supercron_registry()->get_module_controller_path())),
                    supercron_registry_class::controller_path_is_custom      => t('Other place')
            ),
    );

    $form['invocation'][supercron_registry_class::prefs_controller_custom_path] = array(
            '#type'      => 'textfield',
            '#default_value' => supercron_registry()->get_custom_controller_path(),);

    $form['invocation']['help'] = array(
            '#type'  => 'markup',
            '#value' => strtr(t('[div]If you place [tt]@controller[/tt] anywhere but in the Drupal root, make sure to edit the value of [tt]$drupal_dir[/tt] in [tt]@controller[/tt] so that it contains the path to the Drupal root directory.[/div]', array('@controller' => supercron_registry_class::controller_name)), '[]', '<>')
    );

    $form['invocation']['submit'] = array(
            '#type'  => 'submit',
            '#value' => t('Save changes'),
    );

    $exe .= ' -q ';
    $form['invocation']['general'] = array(
            '#type'  => 'textfield',
            '#title' => t('Crontab command line for SuperCron'),
            '#size'  => 100,
            '#value' => $exe . supercron_registry()->get_controller_script_path(),
            '#field_suffix' => l(t('Copy to clipboard'), '', array('attributes' => array(
                    'class' => 'supercron-crontab-command',
                    'rel' => $exe,
            ))),
    );
    $modules = supercron_modules()->modules;

    $markup = '';

    foreach ($modules as $module) {
        $value = $exe . $module->get_call_path();
        $form['commands'][] = array(
                '#type'  => 'textfield',
                '#title' => $module->module_name,
                '#value' => $value,
                '#field_suffix' => l(t('Copy to clipboard'), '#', array('attributes' => array(
                        'class' => 'supercron-crontab-command',
                        'rel' => $value,
                ))),
        );
    }

    if (!$exefound) {
        $message = t('Replace %placeholder by the path to your php interpreter executable');

        $form['replace'] = array(
                '#type'  => 'markup',
                '#value' => t($message, array('%placeholder' => supercron_registry()->get_php_binary_path_description_placeholder())),
        );
    }

    return $form;
}

function supercron_invocation_form_submit($form, &$form_state) {
    supercron_registry()->set_controller_path_directory($form_state['values'][supercron_registry_class::prefs_controller_path], $form_state['values'][supercron_registry_class::prefs_controller_custom_path]);
}

/**
 * Theming function for invocation form.
 */
function theme_supercron_invocation_form($form) {
    $output = '';

    $output .= drupal_render($form[supercron_registry_class::prefs_controller_path]);
    $output .= drupal_render($form[supercron_registry_class::prefs_controller_custom_path]);
    $output .= drupal_render($form['invocation']);

    if (!empty($form['commands'])) {
        $header[] = t('Module');
        $header[] = t('Command line');
        $rows = array();

        foreach (element_children($form['commands']) as $key) {
            $command = &$form['commands'][$key];
            $module  = $command['#title'];
            $command['#title'] = '';
            $rows[] = array($module, drupal_render($command));
        }

        $caption  = t('Expert users who do not wish to use multitasking can use these command lines to call individual cron hooks one at a time.');

        $output .= theme('table', $header, $rows, array(), $caption);
    }

    return $output . drupal_render($form);
}
