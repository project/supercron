<?php
/**
 * @file
 * User supercron installation
 */

include_once('supercron.classes.inc');

/**
 * Implement hook_schema().
 */
function supercron_schema() {
  $schema['supercron_enabled_mods'] = array(
    'fields' => array(
      'id'                  => array('type' => 'serial', 'not null' => TRUE),
      'module_name'         => array('type' => 'varchar', 'length' => 64, 'not null' => TRUE),
      'weight'              => array('type' => 'int', 'not null' => TRUE),
      'rule'                => array('type' => 'varchar', 'length' => '256'),
      'output'              => array('type' => 'text', 'size' => 'big', 'not null' => TRUE),
      'enabled'             => array('type' => 'int', 'not null' => TRUE, 'default' => 1),
      'timestamp'           => array('type' => 'int', 'not null' => TRUE, 'default' => 0),
      'last_exec_interval'  => array('type' => 'int', 'not null' => TRUE, 'default' => 0),
      'total_exec_interval' => array('type' => 'int', 'not null' => TRUE, 'default' => 0),
      'times_count'         => array('type' => 'int', 'size' => 'big', 'not null' => TRUE, 'default' => 0),
      'detached'            => array('type' => 'int', 'not null' => TRUE, 'default' => 0),
      'executing'           => array('type' => 'int', 'not null' => TRUE, 'default' => 0),
    ),
    'primary key' => array('id'),
  );

  $schema['supercron_ips'] = array(
    'fields' => array(
      'iid' => array('type' => 'serial', 'not null' => TRUE),
      'ip'  => array('type' => 'text', 'size' => 'big', 'not null' => TRUE),
    ),
    'primary key' => array('iid'),
  );
  
  return $schema;
}

/**
 * Necessary to avoid some attack scenarios where a safe URL or safety token is pre-planted in the variables table
 */

function supercron_delete_all_variables() {
  supercron_registry_class::clear_registry();
}

/**
 * Implement hook_install().
 */
function supercron_install() {

  // Better be safe
  supercron_delete_all_variables();

  // Install supercron schema.
  drupal_install_schema('supercron');

  // Create a random safety token.
  $ignore = supercron_registry()->get_safety_variable();
  
  // Insert host IP address to supercon_ips table -- Allow cron run requests from these IPs
  foreach (supercron_firewall_class::get_local_ips() as $ip) supercron_firewall()->add_ip($ip);
  
  // First module settings; will automatically create the current list of modules on object construction
  supercron_modules();
  
  // Import Elysia Cron schedule settings if they exist
  $modules = supercron_modules()->get_list_of_cron_enabled_modules();
  foreach ($modules as $module)
  {
      $schedule = variable_get('ec_' . $module . '_cron_rul', '');
      $disabled = variable_get('ec_' . $module . '_cron_d', 0) == '1';
      if (!empty($schedule))
      {
        db_query('UPDATE {supercron_enabled_mods} SET rule=\'%s\', enabled=%d WHERE module_name = \'%s\' LIMIT 1', array($schedule, !$disabled, $module));
      }

  }

  // Keep Poormanscron settings is they exist
  $safe_treshold = variable_get('cron_safe_threshold', 180);
  supercron_registry()->set_autocron_treshold($safe_treshold);
  $poormanscron_was_here = variable_get('cron_safe_threshold', 0) != 0;
  supercron_registry()->set_autocron_to_enabled($poormanscron_was_here); // otherwise, we default to FALSE as autocron must be specifically enabled
  
}

/**
 * Implement hook_uninstall().
 */
function supercron_uninstall() {
  // Uninstall supercron schema.
  drupal_uninstall_schema('supercron');

  // Remove no longer needed variables.
  supercron_delete_all_variables();
}

/**
 * Update script to add 'rule' column to supercron_enabled_mods table.
 */
function supercron_update_1() {
  $rets = array();
  $field = array(
    'type'     => 'varchar',
    'length'   => '256',
    'not null' => TRUE,
  );
  db_add_field($rets, 'supercron_enabled_mods', 'rule', $field);

  $field = array(
    'type'     => 'int',
    'not null' => TRUE,
      'default' => 0,
  );
  db_add_field($rets, 'supercron_enabled_mods', 'executing', $field);

  return $rets;
}
